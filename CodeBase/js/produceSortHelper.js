/*
	Function sortProduce is used to sort what produce is shown by changing inline styling. It hides all other produce types that the user does not select in 
		the produceType_selector.
*/
function sortProduce() {

	$('*').removeAttr('style');
    $('*').prop('style');


    var x = document.getElementById("produceType_selector").value;
    var elementsToHide = [];

    if (x == "Fruit") {
    	var vegElementsToHide = document.getElementsByClassName("produceType_Vegetable");
    	var otherElementsToHide = document.getElementsByClassName("produceType_Other");

    	var v;
		for (v = 0; v < vegElementsToHide.length; v++) {
    		elementsToHide.push(vegElementsToHide[v]);
		}

		var o;
		for (o = 0; o < otherElementsToHide.length; o++) {
    		elementsToHide.push(otherElementsToHide[o]);
		}

    } else if (x == "Vegetable") {
    	var fruitElementsToHide = document.getElementsByClassName("produceType_Fruit");
    	var otherElementsToHide = document.getElementsByClassName("produceType_Other");

    	var f
		for (f = 0; f < fruitElementsToHide.length; f++) {
    		elementsToHide.push(fruitElementsToHide[f]);
		}

		var o;
		for (o = 0; o < otherElementsToHide.length; o++) {
    		elementsToHide.push(otherElementsToHide[o]);
		}

    } else {
    	var fruitElementsToHide = document.getElementsByClassName("produceType_Fruit");
    	var vegElementsToHide = document.getElementsByClassName("produceType_Vegetable");

    	var f
		for (f = 0; f < fruitElementsToHide.length; f++) {
    		elementsToHide.push(fruitElementsToHide[f]);
		}

		var v;
		for (v = 0; v < vegElementsToHide.length; v++) {
    		elementsToHide.push(vegElementsToHide[v]);
		}

    }

    var i;
	for (i = 0; i < elementsToHide.length; i++) {
    	elementsToHide[i].style.display = "none";
	}

}