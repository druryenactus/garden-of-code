function sendProduceId (id, imageName) {
	document.getElementById("modalYesButton").addEventListener("click", function(){
	  var theForm, newInput1, newInput2;
	  // Start by creating a <form>
	  theForm = document.createElement('form');
	  theForm.action = '../../includes/delete_produce.php';
	  theForm.method = 'post';
	  // Next create the <input>s in the form and give them names and values
	  newInput1 = document.createElement('input');
	  newInput1.type = 'hidden';
	  newInput1.name = 'produce_id';
	  newInput1.value = id.slice(7);

	  newInput2 = document.createElement('input');
	  newInput2.type = 'hidden';
	  newInput2.name = 'image_name';
	  newInput2.value = imageName;

	  // Now put everything together...
	  theForm.appendChild(newInput1);
	  theForm.appendChild(newInput2);
	  // ...and it to the DOM...
	  document.getElementById('hidden_form_container').appendChild(theForm);
	  // ...and submit it
	  theForm.submit();

	});
}
