<?php

include_once '../../includes/db_connect.php';
include_once '../../includes/functions.php';

sec_session_start();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Produce Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/view-profile/guest-viewProfile.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/logo/logo.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>


<body>
<?php include("../../includes/header.php"); ?>
<div class="container">
    <h1>View Profile</h1>
    <h1>

        
         <?php $sql = $sql="SELECT email, username, profileimg_name FROM members WHERE email = '{$_GET['email']}'"; 
           $result = $mysqli->query($sql);
        // echo $result;
           if ($result -> num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                $email = $row["email"];
                $username = $row["username"];
                $image = $row["profileimg_name"];  
              }
           }
           else {
            echo "0 results";
           }
        ?>
       </h1>

  	<hr>
	<div class="row">
      <!-- left column -->
      <div class="col-md-3">
        <div class="text-center">
            <?php $sql = $sql="SELECT profileimg_name, email, firstname, lastname, phonenumber, address, companyname FROM members WHERE email = '{$_GET['email']}'"; 
           $result = $mysqli->query($sql);
        // echo $result;
           if ($result -> num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
               echo '<img src="../../assets/profile_images/' . $row['profileimg_name'] . '" id="profilePic" class="avatar img-circle" alt="avatar">';

                echo '</div>';
      echo '</div>';
      
      echo '<!-- edit form column -->';
      echo '<div class="col-md-9 personal-info">';
     
        echo '<h3>Personal info</h3>';
        
          echo '<div>';
            echo '<label>Email:&nbsp;</label>';
                echo $_GET['email'];
          echo '</div>';
          echo '<div>';
            echo '<label>First Name:&nbsp;</label>';
              echo $row['firstname'];
          echo '</div>';
          echo '<div>';
            echo '<label>Last Name:&nbsp;</label>';
              echo $row['lastname'];
          echo '</div>';
          echo '<div>';
            echo '<label>Phone Number:&nbsp;</label>';
              echo $row['phonenumber'];
          echo '</div>';
          echo '<div>';
            echo '<label>Address:&nbsp;</label>';
              echo $row['address'];
          echo '</div>';
          echo '<div>';
            echo '<label>Company:&nbsp;</label>';
              echo $row['companyname'];
          echo '</div>';
      echo '</div>';
              }
           }
           else {
            echo '<img src="//placehold.it/100" class="avatar img-circle" alt="avatar">';
          echo '<h6>No Photo Given</h6>';

           echo '</div>';
      echo '</div>';
      
      echo '<!-- edit form column -->';
      echo '<div class="col-md-9 personal-info">';
     
        echo '<h3>Personal info</h3>';
        
          echo '<div>';
            echo '<label>Email:</label>';
                $row['email'];
          echo '</div>';
          echo '<div>';
            echo '<label>First Name:</label>';
              echo $row['firstname'];
          echo '</div>';
          echo '<div>';
            echo '<label>Last Name:</label>';
              echo $row['lastname'];
          echo '</div>';
          echo '<div>';
            echo '<label>Phone Number:</label>';
              echo $row['phonenumber'];
          echo '</div>';
          echo '<div>';
            echo '<label>Address:</label>';
              echo $row['address'];
          echo '</div>';
          echo '<div>';
            echo '<label>Company:</label>';
              echo $row['companyname'];
          echo '</div>';
      echo '</div>';
           }
       
      ?>

  </div>
</div>
<hr>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <script src="../../js/logo/logo.js"></script>
  </body>
</html>