 <?php

include_once '../../includes/db_connect.php';
include_once '../../includes/functions.php';

sec_session_start();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Produce Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/edit-profile/editProfile.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/logo/logo.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  </head>


<body>
<?php include("../../includes/header.php"); ?>

<div class="container">
    <h1>Edit Profile</h1>
  	<hr>
	

        <form class="form-horizontal" role="form" action="../../includes/update_profile.php" method="post" enctype="multipart/form-data">

          <div class="row">
      <!-- left column -->
      <div class="col-md-3">
        <div class="text-center">
          <?php $sql = $sql="SELECT profileimg_name FROM members WHERE username = '{$_SESSION['username']}'"; 
           $result = $mysqli->query($sql);
        // echo $result;
           if ($result -> num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
               echo '<img src="../../assets/profile_images/' . $row['profileimg_name'] . '" id="profilePic" class="avatar img-circle" alt="avatar">';
               echo '<h6>Upload a different photo...</h6>';
               echo '<input type="file" class="form-control" name="fileToUpload">';
              }
           }
           else {
            echo '<img src="//placehold.it/100" class="avatar img-circle" alt="avatar">';
          echo '<h6>No Photo Given</h6>';
          echo '<input type="file" class="form-control" name="fileToUpload">';
           }
        ?>
        </div>
      </div>
      
      <!-- edit form column -->
      <div class="col-md-9 personal-info">
        
        <h3>Personal info</h3>

          <div class="form-group">
            <label class="col-lg-3 control-label">Email:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" name="profile_email" value='<?php echo $_SESSION['email']; ?>'>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Username:</label>
            <div class="col-md-8">
              <input class="form-control" type="text"  name="profile_username" value='<?php echo $_SESSION['username']; ?>'>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">First Name:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" name="profile_firstname" value='<?php echo $_SESSION['firstname']; ?>'>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Last Name:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" name="profile_lastname" value='<?php echo $_SESSION['lastname']; ?>'>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Phone Number:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" name="profile_phonenumber" value='<?php echo $_SESSION['phonenumber']; ?>'>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Address:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" name="profile_address" value='<?php echo $_SESSION['address']; ?>'>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Company:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" name="profile_companyname" value='<?php echo $_SESSION['companyname']; ?>'>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input type="submit" class="btn btn-primary" value="Save Changes">
              <span></span>
              <a href="/ProducePortal/pages/profile/view-profile.php"><input type="button" class="btn btn-default" value="Cancel"></a>
            </div>
          </div>
        </form>
      </div>
  </div>
</div>
<hr>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <script src="../../js/logo/logo.js"></script>
  </body>
</html>