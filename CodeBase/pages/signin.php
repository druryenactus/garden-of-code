<?php

include_once '../includes/db_connect.php';
include_once '../includes/functions.php';

sec_session_start();

if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script type="text/JavaScript" src="../js/sha512.js"></script> 
    <script type="text/JavaScript" src="../js/forms.js"></script>

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="../../favicon.ico"> -->

    <title>Secure Login: Log In</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/signin/signin.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/logo/logo.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  <?php
      if (isset($_GET['error'])) {
        echo '<p class="error">Error Logging In!</p>';
      }
    ?>
    <?php include("../includes/header.php"); ?>
    <div class="login_container">

      <form class="form-signin" action="../includes/process_login.php" method="post" name="login_form">
        <label for="inputEmail" class="sr-only">Email address</label>
              <input type="text" name="email" id="email" class="form-control" placeholder="Email address" required autofocus>
              <!-- <label for="password" class="sr-only">Password</label> -->
              <input type="password" name="password" id="password" class="form-control" placeholder="Password" onkeypress="handle(event)" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <input class = "btn btn-lg btn-primary" id="loginButton" type="button" value="Login" onclick="formhash(this.form, this.form.password);" />
              <!-- <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button> -->
              <a href="/ProducePortal" class="btn btn-lg btn-danger" id="cancelButton">Cancel</a>
      </form>

      <h3><b>If you don't have a login, please <a href="register/register.php">register</a></b></h3>
            <!-- <p>If you are done, please <a href="../includes/logout.php">log out</a>.</p>
            <p>You are currently logged <?php echo $logged ?>.</p> -->
    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../assets/js/ie10-viewport-bug-workaround.js"></script>

    <script src="../js/logo/logo.js"></script>
  </body>
</html>
