<?php
/**
 * Copyright (C) 2013 peredur.net
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once '../../includes/register.inc.php';
include_once '../../includes/functions.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Registration Form</title>
        <script type="text/JavaScript" src="../../js/sha512.js"></script> 
        <script type="text/JavaScript" src="../../js/forms.js"></script>
        <link rel="stylesheet" href="../../styles/main.css" />

      <!-- Bootstrap core CSS -->
          <link href="../../css/bootstrap.min.css" rel="stylesheet">

          <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
          <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

          <!-- Custom styles for this template -->
          <link href="../../css/register/register.css" rel="stylesheet">

          <!-- Custom styles for this template -->
          <link href="../../css/logo/logo.css" rel="stylesheet">

          <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
          <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
          <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

          <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
          <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->

    </head>
    <body>
    <?php include("../../includes/header.php"); ?>

          <div class="login_container">
                  <!-- Registration form to be output if the POST variables are not
              set or if the registration script caused an error. -->
              <h1 style="text-align: center;">Register Your Produce Portal Account</h1><br>
              <?php
              if (!empty($error_msg)) {
                  echo $error_msg;
              }
              ?>
              <form method="post" class="form-horizontal" name="registration_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>">
                <fieldset>
                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">* Username:</label>  
                  <div class="col-md-4">
                  <input id="username" name="username" type="text" class="form-control input-md" data-toggle="popover" title="Requirements" data-content="Usernames may contain only digits, upper and lower case letters, and underscores" data-trigger="focus">
                    
                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">* Email:</label>  
                  <div class="col-md-4">
                  <input id="email" name="email" type="text" class="form-control input-md" data-toggle="popover" title="Requirements" data-content="Emails must have a valid email format (i.e. username@email.com)" data-trigger="focus">
                    
                  </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">* Password:</label>
                  <div class="col-md-4">
                     <input id="password" name="password" type="password" class="form-control input-md" data-toggle="popover" title="Requirements" data-content="At least 6 characters long.
                    At least one upper case letter (A..Z).
                    At least one lower case letter (a..z).
                    At least one number (0..9).

" data-trigger="focus">
                    
                  </div>
                </div>

                <!-- Confirm Password input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">* Confirm Password:</label>
                  <div class="col-md-4">
                   <input id="confirmpwd" name="confirmpwd" type="password" class="form-control input-md" data-toggle="popover" title="Requirements" data-content="Must match previous password exactly" data-trigger="focus">
                    
                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">First Name:</label>  
                  <div class="col-md-4">
                  <input id="firstname" name="firstname" type="text" class="form-control input-md">
                    
                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">Last Name:</label>  
                  <div class="col-md-4">
                  <input id="lastname" name="lastname" type="text" class="form-control input-md">
                    
                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">Phone Number:</label>  
                  <div class="col-md-4">
                  <input id="phonenumber" name="phonenumber" type="text" class="form-control input-md">
                    
                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">Address:</label>  
                  <div class="col-md-4">
                  <input id="address" name="address" type="text" class="form-control input-md">
                    
                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">Company Name:</label>  
                  <div class="col-md-4">
                  <input id="companyname" name="companyname" type="text" class="form-control input-md">
                    
                  </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="user_type">Producer or Consumer</label>
                  <div class="col-md-4">
                    <select id="user_type" name="user_type" class="form-control">
                      <option value="-1">Select</option>
                      <option value="Producer">Producer</option>
                      <option value="Consumer">Consumer</option>
                    </select>
                  </div>
                </div>

                <!-- Select Basic -->
                <!-- <div class="form-group">
                  <label class="col-md-4 control-label" for="blood_group">Blood Group</label>
                  <div class="col-md-4">
                    <select id="blood_group" name="blood_group" class="form-control">
                      <option value="-1">Select</option>
                      <option value="1">A+</option>
                      <option value="2">B+</option>
                      <option value="3">AB+</option>
                      <option value="4">O+</option>
                      <option value="5">A-</option>
                      <option value="6">B-</option>
                      <option value="7">AB-</option>
                      <option value="8">O-</option>
                    </select>
                  </div>
                </div> -->

                <!-- Button -->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="signup"></label>
                  <div class="col-md-4">
                    <input type="button" 
                         value="Register"
                         class = "btn btn-success" 
                         onclick="return regformhash(this.form,
                                         this.form.username,
                                         this.form.email,
                                         this.form.password,
                                         this.form.confirmpwd,
                                         this.form.firstname,
                                         this.form.lastname,
                                         this.form.phonenumber,
                                         this.form.address,
                                         this.form.companyname,
                                         this.form.user_type
                                         );" /> 
                  </div>
                </div>

                </fieldset>
                </form>
              <p> * Required Field </p>

          </div>
    
        

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <script src="../../js/logo/logo.js"></script>

    <script>
      $(document).ready(function(){
          $('[data-toggle="popover"]').popover();
      });
    </script>
    </body>
</html>
