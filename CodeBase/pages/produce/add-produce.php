<?php

include_once '../../includes/db_connect.php';
include_once '../../includes/functions.php';

sec_session_start();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Produce Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/edit-produce/navbar-fixed-top.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/logo/logo.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Date Picker -->
        <!-- Code Provided By: https://github.com/dbushell/Pikaday -->
    <link rel="stylesheet" href="../../assets/Pikaday-master/css/pikaday.css">
  </head>


<body>
<?php include("../../includes/header.php"); ?>
<div class="container">
    <h1>Add Produce</h1>
  	<hr>
     <form class="form-horizontal" role="form" action="../../includes/insert_produce.php" method="post" enctype="multipart/form-data"> 
	<div class="row">
      <!-- left column -->
      <div class="col-md-3">
        <div class="text-center">
          <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
          <h6>Upload a different photo...</h6>
          
          <input type="file" class="form-control" name="fileToUpload">
        </div>
      </div>
      
      <!-- edit form column -->
      <div class="col-md-9 personal-info">
       <!-- <div class="alert alert-info alert-dismissable">
          <a class="panel-close close" data-dismiss="alert">×</a> 
          <i class="fa fa-coffee"></i>
          This is an <strong>.alert</strong>. Use this to show important messages to the user.
        </div>-->
        <h3>Personal info</h3> 

     
         <div class="form-group"> 
          <label class="col-lg-3 control-label">Produce Type:</label> 
           <div class="col-lg-8"> 
              <?php include("sortProduceList.php"); ?>
           </div> 
       </div> 
         <div class="form-group"> 
            <label class="col-md-3 control-label">Produce Species:</label> 
         <div class="col-md-8"> 
            <input class="form-control" type="text"  name="produce_species"> 
         </div> 
         </div> 
         <div class="form-group"> 
           <label class="col-lg-3 control-label">Plant Quantity:</label> 
             <div class="col-lg-8"> 
               <input class="form-control" type="text" name="plant_quantity"> 
             </div> 
           </div> 
          <div class="form-group">  
             <label class="col-lg-3 control-label">Expected Sell Date:</label> 
             <div class="col-lg-8">
                    <input type="text" id="datepicker" name="sell_date">
              </div>
           </div>
          <div class="form-group">  
             <label class="col-lg-3 control-label">Details About This Produce:</label> 
             <div class="col-lg-8">
                    <textarea class="form-control" name="plant_bio"></textarea>
              </div>
           </div>
           <?php 
           echo '<div class="form-group" style="display:none">'; 
             echo '<label class="col-lg-3 control-label"></label>'; 
             echo '<div class="col-lg-8">'; 
                echo '<input class="form-control" type="text" name="producer_email" value="' . $_SESSION['email'] . '">'; 
             echo '</div>'; 
           echo '</div>'; 
           ?>
           <div class="form-group"> 
             <label class="col-md-3 control-label"></label> 
               <div class="col-md-8"> 
               <input type="submit" class="btn btn-primary" value="Save Changes"> 
               <span></span> 
               <a href="/ProducePortal/pages/produce/view-produce.php"><input type="button" class="btn btn-default" value="Cancel"></a> 
               </div> 
          </div> 
        </form> 
      </div>
  </div>
</div>
<hr>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- Date Picker -->
        <!-- Code Provided By: https://github.com/dbushell/Pikaday -->
    <script src="../../assets/js/moment.js"></script>
    <script src="../../assets/Pikaday-master/pikaday.js"></script>
    <script>
       var picker = new Pikaday({
        field: document.getElementById('datepicker'),
        format: 'YYYY-MM-DD',
        onSelect: function() {
            console.log(this.getMoment().format('Do MMMM YYYY'));
        }
    });
    </script>

    <script src="../../js/logo/logo.js"></script>
  </body>
</html>