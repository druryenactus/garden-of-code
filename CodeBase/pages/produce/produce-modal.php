<?php
include_once '../../includes/db_connect.php';
include_once '../../includes/functions.php';
sec_session_start();
//Sending form data to sql db.
//$email = $_SESSION['email'];

header("Content-type: application/json");

class produceObject implements JsonSerializable {
  public $produceObj_species;
  public $produceObj_email;
  public $produceObj_imgPath;
  public $produceObj_firstname;
  public $produceObj_lastname;
  public $produceObj_plantquantity;
  public $produceObj_plantbio;
  public $produceObj_selldate;

  function __construct($pObj_species, $pObj_email, $pObj_imgPath, $pObj_fname, $pObj_lname, $pObj_qty, $pObj_plantbio, $pObj_selldate) {
    $this->produceObj_species = $pObj_species;
    $this->produceObj_email = $pObj_email;
    $this->produceObj_imgPath = $pObj_imgPath;
    $this->produceObj_firstname = $pObj_fname;
    $this->produceObj_lastname = $pObj_lname;
    $this->produceObj_plantquantity = $pObj_qty;
    $this->produceObj_plantbio = $pObj_plantbio;
    $this->produceObj_selldate = $pObj_selldate;

  }

  public function jsonserialize() {
    return ['producespecies'=>$this->produceObj_species,
            'produceremail'=>$this->produceObj_email,
            'produceimgpath'=>$this->produceObj_imgPath,
            'producerfname'=>$this->produceObj_firstname,
            'producerlname'=>$this->produceObj_lastname,
            'produceqty'=>$this->produceObj_plantquantity,
            'producebio'=>$this->produceObj_plantbio,
            'produceselldate'=>$this->produceObj_selldate
            ];
  }

}

$produceid = $_POST['produceId'];

$sql = $sql="SELECT produce_species, image_name, firstname, lastname, plant_quantity, plant_bio, sell_date, email  FROM produce p, members m WHERE p.producer_email = m.email AND p.produce_id = " . $produceid; 
$result = $mysqli->query($sql);

if ($result -> num_rows > 0) {
  while ($row = $result->fetch_assoc()) {

    $produceObj = new produceObject($row['produce_species'], $row['email'], "../../assets/produce_images/" . $row['image_name'], $row['firstname'], $row['lastname'], $row['plant_quantity'], $row['plant_bio'], $row['sell_date']);


  }
}

//$image_name = $_POST['image_name'];
//$mysqli->query("DELETE FROM produce WHERE `producer_email`='".$email."' AND `produce_id`='".$produceid."' ");
//unlink("../assets/produce_images/" . $image_name);

echo json_encode($produceObj);
//print_r($_POST);
?>

