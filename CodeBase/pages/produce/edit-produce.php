<?php

include_once '../../includes/db_connect.php';
include_once '../../includes/functions.php';

sec_session_start();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Produce Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/edit-produce/navbar-fixed-top.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/logo/logo.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  
  <!-- Date Picker -->
        <!-- Code Provided By: https://github.com/dbushell/Pikaday -->
    <link rel="stylesheet" href="../../assets/Pikaday-master/css/pikaday.css">
  </head>


<body>
<?php include("../../includes/header.php"); ?>
<div class="container">
    <h1>Edit Produce</h1>
  	<hr>
    <?php 
      $sql = $sql="SELECT * FROM produce WHERE produce_id = '{$_GET['produce_id']}'"; 
      $result = $mysqli->query($sql);

      if ($result -> num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                  
                 echo '<form class="form-horizontal" role="form" action="../../includes/update_produce.php" method="post" enctype="multipart/form-data">';

                 echo '<div class="row">';
            echo     '<!-- left column -->';
             echo   '<div class="col-md-3">';
               echo   '<div class="text-center">';
                   echo '<img src="../../assets/produce_images/'. $row['image_name'] . '" class="avatar img-circle" alt="avatar">';
                    echo '<h6>Upload a different photo...</h6>';
                    
                    echo '<input type="file" class="form-control" name="fileToUpload">';
                  echo '</div>';
                echo '</div>';
                
                echo '<!-- edit form column -->';
                echo '<div class="col-md-9 personal-info">';
                  echo '<div class="alert alert-info alert-dismissable">';
                   echo '<a class="panel-close close" data-dismiss="alert">×</a>'; 
                    echo '<i class="fa fa-coffee"></i>';
                    echo 'This is an <strong>.alert</strong>. Use this to show important messages to the user.';
                  echo '</div>';
                  echo '<h3>Personal info</h3>';

                    echo '<div class="form-group">';
                      echo '<label class="col-lg-3 control-label">Produce Type:</label>';
                      echo '<div class="col-lg-8">';
                        echo'<input class="form-control" type="text" name="produce_type" value="' . $row['produce_type'] . '">';
                      echo '</div>';
                    echo '</div>';
                    echo '<div class="form-group">';
                      echo ' <label class="col-md-3 control-label">Produce Species:</label>';
                      echo '<div class="col-md-8">';
                        echo '<input class="form-control" type="text"  name="produce_species" value="' . $row['produce_species'] . '">';
                      echo '</div>';
                    echo '</div>';
                    echo '<div class="form-group">';
                      echo '<label class="col-lg-3 control-label">Plant Quantity:</label>';
                      echo '<div class="col-lg-8">';
                        echo '<input class="form-control" type="text" name="plant_quantity" value="' . $row['plant_quantity'] . '">';
                      echo '</div>';
                    echo '</div>';
                   echo '<div class="form-group"> ';
                      echo '<label class="col-lg-3 control-label">Expected Sell Date:</label>';
                      echo '<div class="col-lg-8">';
                        echo '<input type="text" id="datepicker" name="sell_date" value="' . $row['sell_date'] . '">';
                      echo '</div>';
                    echo '</div>';
                    echo '<div class="form-group">';  
                      echo '<label class="col-lg-3 control-label">Details About This Produce:</label>';
                        echo '<div class="col-lg-8">';
                          echo '<textarea class="form-control" name="plant_bio">' . $row['plant_bio'] . '</textarea>';
                            echo '</div>';
                          echo '</div>';
                    echo '<div class="form-group" style="display:none">';
                      echo '<label class="col-lg-3 control-label">Produce Id:</label>';
                      echo '<div class="col-lg-8">';
                        echo'<input class="form-control" type="text" name="produce_id" value="' . $row['produce_id'] . '">';
                      echo '</div>';
                    echo '</div>';
                    echo '<div class="form-group">';
                      echo '<label class="col-md-3 control-label"></label>';
                        echo '<div class="col-md-8">';
                        echo '<input type="submit" class="btn btn-primary" value="Save Changes">';
                        echo '<span></span>';
                        echo '<a href="/ProducePortal/pages/produce/view-produce.php"><input type="button" class="btn btn-default" value="Cancel"></a>';
                        echo '</div>';
                   echo '</div>';
                 echo '</form>';
     }
   }
           else {
            echo '0 results';
           }
        ?>
      </div>
  </div>
</div>
<hr>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  
    <!-- Date Picker -->
            <!-- Code Provided By: https://github.com/dbushell/Pikaday -->
        <script src="../../assets/js/moment.js"></script>
        <script src="../../assets/Pikaday-master/pikaday.js"></script>
        <script>
           var picker = new Pikaday({
            field: document.getElementById('datepicker'),
            format: 'YYYY-MM-DD',
            onSelect: function() {
                console.log(this.getMoment().format('Do MMMM YYYY'));
            }
        });
        </script>

        <script src="../../js/logo/logo.js"></script>
  </body>
</html>