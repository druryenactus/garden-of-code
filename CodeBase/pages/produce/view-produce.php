<?php

include_once '../../includes/db_connect.php';
include_once '../../includes/functions.php';

sec_session_start();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="../../favicon.ico"> -->

    <title>Produce Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/view-produce/viewProduce.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/logo/logo.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>


<body>
<?php include("../../includes/header.php"); ?>
<div class="container">
    <h1>Your Produce</h1>

<!-- Gathered from http://bootsnipp.com/snippets/QbOK1 -->
    <div class="container-fluid">
      <div class="row">
         <?php $sql = $sql="SELECT * FROM produce WHERE producer_email = '{$_SESSION['email']}'"; 
           $result = $mysqli->query($sql);
        // echo $result;
           // <a href="../../includes/delete_produce.php?produce_id=' . $row['produce_id'] . '"' . 'class="btn btn-danger btm-sm" data-toggle="modal" data-target="#deleteModal">Delete</a>
           if ($result -> num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                echo '<div class="col-xs-12 col-sm-4 col-md-2">';
                echo '<div class="productbox">';
                echo '<img src="../../assets/produce_images/' . $row['image_name'] . '"class="img-responsive">';
                echo '<div class="producttitle">' . $row['produce_species'] . '</div>';
                echo '<p class="text-justify">' . "Quantity:" . $row['plant_quantity'] . '</p>';
                echo '<address><strong>Producer Email</strong><br>
                        <a href="mailto:#">' . $row['producer_email'] . '</a>
                      </address>';
                echo 
                      '<div class="produceChange">
                        <div class="pull-right">
                          <a class="btn btn-danger btm-sm" id=produce' . $row['produce_id'] . ' data-toggle="modal" data-target="#deleteModal" onClick="sendProduceId(this.id, &quot;' . $row['image_name'] . '&quot;);">Delete</a>
                        </div>
                        <div class="produceEdit">
                          <a href="/ProducePortal/pages/produce/edit-produce.php?produce_id=' . $row['produce_id'] .  '"' .  'class="btn btn-warning btm-sm" role="button">Edit</a>
                        </div>
                    
                      </div>';
                echo '</div>';
                echo '</div>';
              }
           }
           else {
            echo "0 results";
           }
        ?>
        <div class="col-xs-12 col-sm-4 col-md-2" id="addProduce1">
          <div class="productbox" id="addProduce">
            <!-- <img src="orange.jpg" class="img-responsive"> -->
            <a class="btn btn-default btn-circle btn-xl" aria-label="Left Align" href="/ProducePortal/pages/produce/add-produce.php" id="addProducePlus">
              <!-- <span id="addProducePlus" class="glyphicon glyphicon-plus" aria-hidden="true"></span> -->+
            </a>
            <!-- <span id="addProducePlus" class="glyphicon glyphicon-plus"></span> -->
          </div>
        </div>
        <!-- Modal -->
        <div id="hidden_form_container" style="display:none;"></div>
        <div class="modal fade" id="deleteModal" role="dialog">
          <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Are You Sure</h4>
                    </div>
                    <div class="modal-body">
                        <p> Do you wish to delete this item from your produce? </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="modalYesButton" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-danger" id="modalNoButton" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

	<div class="row">
      <!-- left column -->
      
      <!-- edit form column -->
      <div class="col-md-9 personal-info">
        
        <form class="form-horizontal" role="form">
        </form>
      </div>
  </div>
</div>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <script src="../../js/logo/logo.js"></script>
    <script src="../../js/deleteProduceHelper.js"></script>
  </body>
</html>