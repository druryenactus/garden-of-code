<?php

include_once '../../includes/db_connect.php';
include_once '../../includes/functions.php';

sec_session_start();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="../../favicon.ico"> -->

    <title>Produce Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
      
    <!-- FancyBox CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/browse-produce/browseProduce.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/logo/logo.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/logo/logo.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- <script src="//code.jquery.com/jquery-3.2.1.min.js"></script> -->

<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script> -->
    
    </head>
    
<body>
<?php include("../../includes/header.php"); ?>

<div class="container">
    <h1>Our Produce</h1>    

<!-- Gathered from http://bootsnipp.com/snippets/QbOK1 -->
    <div class="container-fluid">
              <?php include("sortProduceList.php"); ?>
      <div class="row">
         <?php $sql = $sql="SELECT * FROM produce"; 
           $result = $mysqli->query($sql);
        // echo $result;
           if ($result -> num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                echo '<a id="' . $row['produce_id'] . '" data-toggle="modal" data-target="#viewModal" data-backdrop="true" data-src="produce-modal.php" onClick="populateModal(this.id);">';  
                // Daniel's FancyBox Example echo '<a class="fancybox_modal" data-fancybox data-type="ajax" data-src="produce-modal.php" href="javascript:;">'; 
                echo '<div class="col-xs-12 col-sm-4 col-md-2 produceType_'. $row['produce_type'] . '">';
                echo '<div class="productbox" id="' . $row['produce_id'] . '"> ';
                echo '<input id="produceform" value="' . $row['produce_id'] . '"> ';  
                echo '<img src="../../assets/produce_images/'. $row['image_name'] . '" class="img-responsive">';
                echo '<div class="producttitle">' . $row['produce_species'] . '</div>';
                echo '<p class="text-justify">' . "Quantity:" . $row['plant_quantity'] . '</p>';
                echo '</div>';
                echo '</div>';
                echo '</a>';  
                echo '</form>';  
              }
           }
           else {
            echo "0 results";
           }
        ?>
      </div>
    </div>
    
    
    
<?php


    echo '<div class="modal fade" id="viewModal" role="dialog">';
          echo '<div class="vertical-alignment-helper">';
            echo '<div class="modal-dialog modal-lg vertical-align-center">';
                echo '<div class="modal-content">';
                    echo '<div class="modal-header">';
                        echo '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                        echo '<h4 class="modal-title" id="modalProduceName"></h4>';
                    echo '</div>';
                    echo '<div class="modal-body">';
                        echo '<div class="row">';
                          echo '<div class="col-md-6">';
                              echo '<img id="modalProduceImg" src="">';
                          echo '</div>';
                          echo '<div class="col-md-6">';
                          echo '<p class="modalContentLabel">Quantity</p>';
                          echo '<p id="modalQuantity"></p>';
                          echo '<p class="modalContentLabel">Info</p>';
                          echo '<p id="modalBio"></p>';
                          echo '<p class="modalContentLabel">Sell Date</p>';
                          echo '<p id="modalSellDate"></p>';
                          echo '</div>';
                        echo '</div>';
                      echo '<p class="modalContentLabel">Grown By: </p><a href="" id="modalProducerName"></a>';
                    echo '</div>';
                    echo '<div class="modal-footer">';
                    echo '</div>';
                echo '</div>';
            echo '</div>';
          echo '</div>';
        echo '</div>';
      echo '</div>';

  ?>
    
  <div class="row">
      <!-- left column -->
      
      <!-- edit form column -->
      <div class="col-md-9 personal-info">
        
        <form class="form-horizontal" role="form">
        </form>
      </div>
  </div>
</div>
     
    
<script>
    
    
    
    </script>

    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="../../js/logo/logo.js"></script>
    <script src="../../js/viewProduceHelper.js"></script>
    <script src="../../js/produceSortHelper.js"></script>
  </body>
</html>