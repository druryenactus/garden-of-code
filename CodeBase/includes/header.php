<?php 
  /* Sets SITE_URL for dev environment on mcs.drury.edu server. */
  if ($_SERVER['SERVER_NAME'] == "mcs.drury.edu") {
    define ('SITE_URL', '//'.$_SERVER['SERVER_NAME'].'/ProducePortal');
  }

  /* Sets SITE_URL for live site server. */
  else {
    define ('SITE_URL', $_SERVER['SERVER_NAME']);
  }

  
?>

<?php if (login_check($mysqli) == true) : ?>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo SITE_URL;?>"> 
                <div class="triangle-right"></div>
                <div class="producebox">Produce</div>
                <div class="triangle-left"></div>
                <div class="portalbox">Portal</div>
                </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo SITE_URL;?>">Home</a></li>
            <li><a href="#about">About</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['firstname']?>'s Account <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo SITE_URL;?>/pages/profile/view-profile.php">Your Profile</a></li>
                <li><a href="<?php echo SITE_URL;?>/pages/profile/edit-profile.php">Edit Profile</a></li>
                <li><a href="<?php echo SITE_URL;?>/pages/produce/view-produce.php">Your Produce</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header"></li>
                <li><a href="<?php echo SITE_URL;?>/includes/logout.php">Sign Out</a></li>

              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <?php else : ?>

      <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo SITE_URL;?>">
              
                <div class="triangle-right"></div>
                <div class="producebox">Produce</div>
                <div class="triangle-left"></div>
                <div class="portalbox">Portal</div>
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo SITE_URL;?>">Home</a></li>
            <li><a href="#about">About</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="<?php echo SITE_URL; ?>/pages/signin.php">Sign In<span class="sr-only">(current)</span></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <?php endif; ?>

