<?php
include_once 'db_connect.php';
include_once 'functions.php';
sec_session_start();
//Sending form data to sql db.
$firstname = filter_var($_POST['profile_firstname'], FILTER_SANITIZE_STRING);
$lastname = filter_var($_POST['profile_lastname'], FILTER_SANITIZE_STRING);
$new_username = filter_var($_POST['profile_username'], FILTER_SANITIZE_STRING);
$email = filter_var($_POST['profile_email'], FILTER_SANITIZE_EMAIL);
$phonenumber = filter_var($_POST['profile_phonenumber'], FILTER_SANITIZE_STRING);
$companyname = filter_var($_POST['profile_companyname'], FILTER_SANITIZE_STRING);
$address = filter_var($_POST['profile_address'], FILTER_SANITIZE_STRING);
$username = filter_var($_SESSION['username'], FILTER_SANITIZE_STRING);

if ($_FILES['fileToUpload']['name'] == null) {

	$mysqli->query("UPDATE members SET `firstname`='".$firstname."', `lastname`='".$lastname."', `username`='".$new_username."', `email`='".$email."', `phonenumber`='".$phonenumber."', `companyname`='".$companyname."', `address`='".$address."' WHERE `username`='".$username."' ");

}
else {

	$currentTime = time();
	$imagename = $currentTime . '_' . $_FILES['fileToUpload']['name'];

	$fileToMove = $_FILES['fileToUpload']['tmp_name'];
	$destination = "../assets/profile_images/" . $currentTime . '_' . $_FILES['fileToUpload']['name'];
	if(move_uploaded_file($fileToMove,$destination)){
	    
	    echo "upload successful.";
	}
	else{
	    echo "upload failed.";
	}

	$mysqli->query("UPDATE members SET `firstname`='".$firstname."', `lastname`='".$lastname."', `username`='".$new_username."', `email`='".$email."', `phonenumber`='".$phonenumber."', `companyname`='".$companyname."', `address`='".$address."', `profileimg_name`='".$imagename."' WHERE `username`='".$username."' ");
}

$_SESSION['username'] = $new_username;
$_SESSION['firstname'] = $firstname;
$_SESSION['lastname'] = $lastname;
$_SESSION['phonenumber'] = $phonenumber;
$_SESSION['address'] = $address;
$_SESSION['lastname'] = $lastname;
$_SESSION['email'] = $email;
$_SESSION['companyname'] = $companyname;
header("Location: ../pages/profile/view-profile.php");
?>